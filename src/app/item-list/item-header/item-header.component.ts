import { Component ,EventEmitter,Input,Output,signal} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Item } from '../item-list.interface';

@Component({
  selector: 'app-item-header',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './item-header.component.html',
  styleUrls: ['./item-header.component.scss']
})
export class ItemHeaderComponent {
  @Input() item: Item = {} as Item;
  @Output() delete = new EventEmitter<Item>();
  @Output() update = new EventEmitter<Item>();
  @Output() insert = new EventEmitter<Item>();

  isEdit = false;

  doUpdateArticle(item: Item) {

    this.update.emit(item);
  };

  async doDeleteArticle() {

    this.delete.emit(this.item);
  };

  doInsertArticle(item: Item) {

    this.insert.emit(item);
  };

}
