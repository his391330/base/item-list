import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Item } from '../item-list.interface';
import { ItemHeaderComponent } from '../item-header/item-header.component';

@Component({
  selector: 'his-item-body',
  standalone: true,
  imports: [CommonModule,ItemHeaderComponent],
  templateUrl: './item-body.component.html',
  styleUrls: ['./item-body.component.scss']
})
export class ItemBodyComponent {
  @Input() item !: Item;
}
