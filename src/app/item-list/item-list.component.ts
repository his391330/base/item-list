import { Component, EventEmitter, Input, Output, WritableSignal,signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemHeaderComponent } from './item-header/item-header.component';
import { ItemBodyComponent } from './item-body/item-body.component';
import { Item } from './item-list.interface';

@Component({
  selector: 'app-item-list',
  standalone: true,
  imports: [CommonModule,ItemHeaderComponent,ItemBodyComponent],
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})

export class ItemListComponent {
  @Input() item: WritableSignal<Item[]> = signal([] as Item[]);
  @Output() delete = new EventEmitter<Item>();

  async onRemoveArticle(item: Item) {

    try {
      await this.articleService.removeArticle(item.id);
      this.item.mutate(x => {
        x.splice(x.findIndex(x => x.id === item.id),1);
      });
    } catch (e) {
      console.error(e);
    }
  };

  async onModifyArticle(item: Item) {

   try {
      await this.articleService.modifyArticle(item);
      this.item.mutate(x => {
        x[x.findIndex(x => x.id === item.id)] = item;
      });
    } catch (e) {
      console.error(e);
    }
  };

}
